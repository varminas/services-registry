## Description
* Services Registry UI
* Service Registry Backend

#Docker
Build docker image:
```bash
mvn clean install dockerfile:build
```

Build containers:
```bash
docker-compose up -d --build
```

Start containers:
```bash
docker-compose up -d

```
Stop containers (-v - delete volumes):
```bash
docker-compose down -v
```
