package lt.arminai.eserviceregistry.eserviceregistry;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.HealthStatus;
import lt.arminai.eserviceregistry.eserviceregistry.repository.EServiceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static lt.arminai.eserviceregistry.eserviceregistry.TestData.createEServiceModel;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EServiceRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EServiceRepository eServiceRepository;

    @Test
    public void whenFindAll_thenReceiveAll() {
        // given
        final EService eService = createEServiceModel();

//        entityManager.persist(eService);
//        entityManager.flush();

        // when
        final List<EService> all = eServiceRepository.findAll();

        // then
        assertThat(all).hasSize(3);
    }
}
