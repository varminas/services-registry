package lt.arminai.eserviceregistry.eserviceregistry;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import lt.arminai.eserviceregistry.eserviceregistry.repository.EServiceRepository;
import lt.arminai.eserviceregistry.eserviceregistry.service.EServiceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class EServiceServiceTests {

    @TestConfiguration
    static class EServiceServiceTestContextConfiguration {
        @Bean
        public EServiceService eServiceService(EServiceRepository eServiceRepository) {
            return new EServiceService(eServiceRepository);
        }
    }

    @Autowired
    private EServiceService eServiceService;

    @MockBean
    private EServiceRepository eServiceRepository;

    @Before
    public void setup() {
        when(eServiceRepository.findAll()).thenReturn(Arrays.asList(
                TestData.createEServiceModel(),
                TestData.createEServiceModel(),
                TestData.createEServiceModel()));
    }

    @Test
    public void whenFindAll_thenFindAll() {
        final List<EService> services = eServiceService.findServices();

        assertThat(services).hasSize(3);
    }
}
