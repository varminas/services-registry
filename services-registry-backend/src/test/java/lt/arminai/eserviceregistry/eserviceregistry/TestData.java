package lt.arminai.eserviceregistry.eserviceregistry;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.HealthStatus;

import java.time.LocalDateTime;
import java.util.Collections;

public final class TestData {

    static EService createEServiceModel() {
        return new EService(
                1234L,
                "Service 1234",
                "Service A is very important",
                "http://www.google.com",
                HealthStatus.UP,
                LocalDateTime.now(),
                Collections.emptyList(),
                Collections.emptySet(),
                Collections.emptySet()
        );
    }

}
