package lt.arminai.eserviceregistry.eserviceregistry;

import lt.arminai.eserviceregistry.eserviceregistry.controller.EServiceController;
import lt.arminai.eserviceregistry.eserviceregistry.service.EServiceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(EServiceController.class)
public class EServiceControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EServiceService eServiceService;

    @Test
    public void whenFindAll_thenFindAll() throws Exception {
        given(eServiceService.findServices()).willReturn(Arrays.asList(TestData.createEServiceModel()));

        mvc.perform(get("/services")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.services", hasSize(1)))
                .andExpect(jsonPath("$.services[0].id", is(1234)));

    }

}
