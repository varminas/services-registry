package lt.arminai.eserviceregistry.eserviceregistry.service;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.EServiceHealth;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.EServiceHealthWrapper;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.HealthStatus;
import lt.arminai.eserviceregistry.eserviceregistry.repository.EServiceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class EServiceService {

    private final EServiceRepository eServiceRepository;

    public EServiceService(EServiceRepository eServiceRepository) {
        this.eServiceRepository = eServiceRepository;
    }

    public List<EService> findServices() {
        return eServiceRepository.findAll();
    }

    public Optional<EService> getApi(Long id) {
        return eServiceRepository.findById(id);
    }

    @Transactional
    public void updateServicesHealthStatus(List<EServiceHealthWrapper> healths) {
        final List<EService> allServices = findServices();

        healths.forEach(wrapper ->
                allServices.stream()
                        .filter(service -> service.getId().equals(wrapper.getServiceId()))
                        .findFirst()
                        .ifPresent(service -> {
                            final ResponseEntity<EServiceHealth> eServiceHealth = wrapper.getEServiceHealth();

                            final HttpStatus statusCode = eServiceHealth.getStatusCode();
                            if (!statusCode.equals(HttpStatus.OK)) {
                                service.setHealthStatus(HealthStatus.DOWN);
                            }

                            final EServiceHealth body = eServiceHealth.getBody();
                            if (body != null) {
                                service.setHealthStatus(body.getStatus());
                            }
                        })
        );
    }
}
