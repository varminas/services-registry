package lt.arminai.eserviceregistry.eserviceregistry.controller.dtos;

public enum HealthStatusDto {
    UP, DOWN, UNKNOWN
}
