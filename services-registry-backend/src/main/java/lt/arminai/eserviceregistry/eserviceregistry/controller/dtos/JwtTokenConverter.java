package lt.arminai.eserviceregistry.eserviceregistry.controller.dtos;

import lt.arminai.eserviceregistry.eserviceregistry.model.JwtToken;

import java.util.Collections;

public final class JwtTokenConverter {

    public static final JwtToken fromDto(JwtTokenDto dto) {
        if (dto == null) {
            return null;
        }
        return new JwtToken(dto.getId(), dto.getName(), dto.getValue(), Collections.emptyList());
    }

    public static final JwtTokenDto toDto(JwtToken jwtToken) {
        if (jwtToken == null) {
            return null;
        }
        return new JwtTokenDto(jwtToken.getId(), jwtToken.getName(), jwtToken.getValue());
    }
}
