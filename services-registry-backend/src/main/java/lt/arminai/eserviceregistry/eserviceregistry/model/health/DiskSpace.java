package lt.arminai.eserviceregistry.eserviceregistry.model.health;

import lombok.Data;

@Data
public class DiskSpace {
    private HealthStatus status;
    private DiskSpaceDetails details;
}
