package lt.arminai.eserviceregistry.eserviceregistry.model.health;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.http.ResponseEntity;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EServiceHealthWrapper {
    private Long serviceId;
    private ResponseEntity<EServiceHealth> eServiceHealth;
}
