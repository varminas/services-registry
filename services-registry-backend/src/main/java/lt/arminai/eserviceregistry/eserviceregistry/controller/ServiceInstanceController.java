package lt.arminai.eserviceregistry.eserviceregistry.controller;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ServiceInstanceController {
    private DiscoveryClient discoveryClient;

    public ServiceInstanceController(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    @GetMapping("service-instances/{applicationName}")
    public List<ServiceInstance> getServiceInstancesByApplicationName(@PathVariable("applicationName") String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
}
