package lt.arminai.eserviceregistry.eserviceregistry.controller.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class EServiceListDto {
    private List<EServiceDto> services = new ArrayList<>();
}
