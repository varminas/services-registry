package lt.arminai.eserviceregistry.eserviceregistry.controller.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class JwtTokenDto {
    private Long id;
    private String name;
    private String value;
}
