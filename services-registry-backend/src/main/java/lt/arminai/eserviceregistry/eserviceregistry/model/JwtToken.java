package lt.arminai.eserviceregistry.eserviceregistry.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "JWT_TOKEN")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class JwtToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "VALUE")
    private String value;

    @ManyToMany(mappedBy = "jwtTokens")
    private List<EService> services = new ArrayList<>();
}
