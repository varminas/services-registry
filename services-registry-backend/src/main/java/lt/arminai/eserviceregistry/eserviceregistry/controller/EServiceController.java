package lt.arminai.eserviceregistry.eserviceregistry.controller;

import lt.arminai.eserviceregistry.eserviceregistry.controller.dtos.EServiceConverter;
import lt.arminai.eserviceregistry.eserviceregistry.controller.dtos.EServiceDto;
import lt.arminai.eserviceregistry.eserviceregistry.controller.dtos.EServiceListDto;
import lt.arminai.eserviceregistry.eserviceregistry.controller.dtos.EServicesConverter;
import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import lt.arminai.eserviceregistry.eserviceregistry.service.EServiceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class EServiceController {

    private final EServiceService eServiceService;

    public EServiceController(EServiceService eServiceService) {
        this.eServiceService = eServiceService;
    }

    @GetMapping("services/{id}")
    public EServiceDto getService(@PathVariable("id") Long id) {
        return eServiceService.getApi(id)
                .map(EServiceConverter::toDto)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Service '%d' not found", id)));
    }

    @GetMapping("services")
    public EServiceListDto findServices() {
        final List<EService> services = eServiceService.findServices();

        return EServicesConverter.toDto(services);
    }

}
