package lt.arminai.eserviceregistry.eserviceregistry.model.health;

import lombok.Data;

@Data
public class Hystrix {
    private HealthStatus status;
}
