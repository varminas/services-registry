package lt.arminai.eserviceregistry.eserviceregistry.model.health;

import lombok.Data;

@Data
public class DiskSpaceDetails {
    private Long total;
    private Long free;
    private Long threshold;
}
