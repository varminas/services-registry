package lt.arminai.eserviceregistry.eserviceregistry.model.health;

import lombok.Data;

@Data
public class Db {
    private String status;
    private DbDetails details;
}
