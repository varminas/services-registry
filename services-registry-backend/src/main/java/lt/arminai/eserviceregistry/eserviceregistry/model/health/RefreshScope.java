package lt.arminai.eserviceregistry.eserviceregistry.model.health;

import lombok.Data;

@Data
public class RefreshScope {
    private HealthStatus status;
}
