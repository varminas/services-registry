package lt.arminai.eserviceregistry.eserviceregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableEurekaClient
@EnableScheduling
@SpringBootApplication
public class EServiceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(EServiceRegistryApplication.class, args);
	}

}
