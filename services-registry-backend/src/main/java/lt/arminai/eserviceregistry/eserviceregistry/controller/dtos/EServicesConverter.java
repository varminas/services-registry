package lt.arminai.eserviceregistry.eserviceregistry.controller.dtos;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;

import java.util.List;
import java.util.stream.Collectors;

public final class EServicesConverter {

    public static final EServiceListDto toDto(List<EService> EServices) {
        if (EServices == null || EServices.isEmpty()) {
            return new EServiceListDto();
        }

        return new EServiceListDto(EServices.stream().map(EServiceConverter::toDto).collect(Collectors.toList()));
    }
}
