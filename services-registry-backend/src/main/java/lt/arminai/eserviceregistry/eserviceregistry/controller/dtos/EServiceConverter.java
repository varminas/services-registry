package lt.arminai.eserviceregistry.eserviceregistry.controller.dtos;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import lt.arminai.eserviceregistry.eserviceregistry.model.JwtToken;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.HealthStatus;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class EServiceConverter {

    public static final EService fromDto(EServiceDto dto) {
        if (dto == null) {
            return null;
        }

        return new EService(
                dto.getId(),
                dto.getName(),
                dto.getDescription(),
                dto.getHealthUrl(),
                HealthStatus.valueOf(dto.getHealthStatus().toString()),
                dto.getUpdatedAt(),
                Collections.emptyList(),
                Collections.emptySet(),
                Collections.emptySet()
        );
    }

    public static final EServiceDto toDto(EService service) {
        if (service == null) {
            return null;
        }

        final List<Long> tokens = service.getJwtTokens().stream()
                .map(JwtToken::getId)
                .collect(Collectors.toList());

        final Set<Long> relationsTo = service.getRelationsTo().stream()
                .map(EService::getId)
                .collect(Collectors.toSet());

        return new EServiceDto(
                service.getId(),
                service.getName(),
                service.getDescription(),
                service.getHealthUrl(),
                HealthStatusDto.valueOf(service.getHealthStatus().toString()),
                service.getUpdatedAt(),
                tokens,
                relationsTo
        );
    }
}
