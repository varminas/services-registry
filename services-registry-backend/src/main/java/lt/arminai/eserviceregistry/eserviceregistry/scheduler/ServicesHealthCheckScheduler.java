package lt.arminai.eserviceregistry.eserviceregistry.scheduler;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.EServiceHealth;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.EServiceHealthWrapper;
import lt.arminai.eserviceregistry.eserviceregistry.service.EServiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Component
public class ServicesHealthCheckScheduler {
    private static final Logger logger = LoggerFactory.getLogger(ServicesHealthCheckScheduler.class);

    private final EServiceService eServiceService;
    private final ExecutorService executorService;
    private final RestTemplate restTemplate;

    public ServicesHealthCheckScheduler(EServiceService eServiceService, ExecutorService executorService, RestTemplate restTemplate) {
        this.eServiceService = eServiceService;
        this.executorService = executorService;
        this.restTemplate = restTemplate;
    }

    // Every 10 minutes (1000ms * 60 * 10)
    @Scheduled(fixedRate = 600000)
    public void receiveServicesHealth() {
        logger.info("Receiving services health status...");

        final List<EService> services = eServiceService.findServices();

        final List<Future<EServiceHealthWrapper>> futures = getFutures(services);

        final List<EServiceHealthWrapper> healths = getEServiceHealthWrappers(futures);

        eServiceService.updateServicesHealthStatus(healths);
        logger.info("Requested total {} services, answers received from {}", services.size(), healths.size());
        logger.info("Services health status received.");
    }

    private List<EServiceHealthWrapper> getEServiceHealthWrappers(List<Future<EServiceHealthWrapper>> futures) {
        return futures.stream()
                .map(future -> {
                    try {
                        return future.get();
                    } catch (Exception e) {
                        logger.error("Error while requesting health of the service", e);
                        return new EServiceHealthWrapper();
                    }
                })
                .filter(wrapper -> wrapper.getServiceId() != null)
                .collect(Collectors.toList());
    }

    private List<Future<EServiceHealthWrapper>> getFutures(List<EService> services) {
        return services.stream()
                .map(service -> executorService.submit(() -> {
                            final ResponseEntity<EServiceHealth> responseEntity = receiveHealthStatus(service);
                            return new EServiceHealthWrapper(service.getId(), responseEntity);
                        })
                ).collect(Collectors.toList());
    }

    private ResponseEntity<EServiceHealth> receiveHealthStatus(EService service) {
        logger.info("Calling {}", service.getHealthUrl());
        return restTemplate.getForEntity(service.getHealthUrl(), EServiceHealth.class);
    }
}
