package lt.arminai.eserviceregistry.eserviceregistry.integration;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

// TODO this is only for demo purpose. need to replace with the real one
@Component
public class ServiceAHealthIndicator implements HealthIndicator {
    private static final String message_key = "Service A";

    @Override
    public Health health() {
        if (!isRunningServiceA()) {
            return Health.down().withDetail(message_key, "Not Available").build();
        }
        return Health.up().withDetail(message_key, "Available").build();
    }

    private Boolean isRunningServiceA() {
        final Boolean isRunning = true;
        // Logic Skipped
        return isRunning;

    }
}
