package lt.arminai.eserviceregistry.eserviceregistry.repository;

import lt.arminai.eserviceregistry.eserviceregistry.model.EService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EServiceRepository extends CrudRepository<EService, Long> {

    @Override
    List<EService> findAll();
}
