package lt.arminai.eserviceregistry.eserviceregistry.model.health;

public enum HealthStatus {
    UP, DOWN, UNKNOWN
}
