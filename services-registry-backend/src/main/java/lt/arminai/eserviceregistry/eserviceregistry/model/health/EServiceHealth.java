package lt.arminai.eserviceregistry.eserviceregistry.model.health;

import lombok.Data;

@Data
public class EServiceHealth {
    private HealthStatus status;
    private Details details;
}
