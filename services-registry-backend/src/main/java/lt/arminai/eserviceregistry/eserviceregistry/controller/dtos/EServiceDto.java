package lt.arminai.eserviceregistry.eserviceregistry.controller.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Getter
public class EServiceDto {
    private Long id;
    private String name;
    private String description;
    private String healthUrl;
    private HealthStatusDto healthStatus;
    private LocalDateTime updatedAt;
    private List<Long> jwtTokens;
    private Set<Long> relationsTo;
}
