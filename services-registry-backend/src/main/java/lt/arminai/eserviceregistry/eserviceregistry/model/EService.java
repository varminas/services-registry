package lt.arminai.eserviceregistry.eserviceregistry.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lt.arminai.eserviceregistry.eserviceregistry.model.health.HealthStatus;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Entity
@Table(name = "SERVICE")
public class EService {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "HEALTH_URL")
    private String healthUrl;

    @Setter
    @Column(name = "HEALTH_STATUS")
    @Enumerated(EnumType.STRING)
    private HealthStatus healthStatus;

    @UpdateTimestamp
    @Column(name = "UPDATED_AT")
    private LocalDateTime updatedAt;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "SERVICE_JWT_TOKEN",
            joinColumns = @JoinColumn(name = "SERVICE_ID"),
            inverseJoinColumns = @JoinColumn(name = "JWT_TOKEN_ID")
    )
    private List<JwtToken> jwtTokens = new ArrayList<>();

    @ManyToMany(cascade={CascadeType.ALL})
    @JoinTable(name="SERVICE_RELATION",
            joinColumns={@JoinColumn(name="RELATION_TO_ID")},
            inverseJoinColumns={@JoinColumn(name="RELATION_FROM_ID")})
    private Set<EService> relationsTo = new HashSet<>();

    @ManyToMany(mappedBy="relationsTo")
    private Set<EService> relationsFrom = new HashSet<>();
}
