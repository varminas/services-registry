package io.openliberty.sample;

import com.netflix.appinfo.ApplicationInfoManager;
import com.netflix.appinfo.EurekaInstanceConfig;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.appinfo.MyDataCenterInstanceConfig;
import com.netflix.appinfo.providers.EurekaConfigBasedInstanceInfoProvider;
import com.netflix.discovery.DefaultEurekaClientConfig;
import com.netflix.discovery.DiscoveryClient;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.EurekaClientConfig;


public class EurekaConfiguration {
    private static ApplicationInfoManager applicationInfoManager;
    private static EurekaClient eurekaClient;

    private static synchronized ApplicationInfoManager initializeApplicationInfoManager(
            EurekaInstanceConfig instanceConfig) {
        if (applicationInfoManager == null) {
            InstanceInfo instanceInfo = new EurekaConfigBasedInstanceInfoProvider(instanceConfig).get();
            applicationInfoManager = new ApplicationInfoManager(instanceConfig, instanceInfo);
        }

        return applicationInfoManager;
    }

    private static synchronized EurekaClient initializeEurekaClient(ApplicationInfoManager applicationInfoManager,
                                                                    EurekaClientConfig clientConfig) {
        if (eurekaClient == null) {
            eurekaClient = new DiscoveryClient(applicationInfoManager, clientConfig);
        }

        return eurekaClient;
    }


    public static EurekaClient getEurekaClient() {
        ApplicationInfoManager applicationInfoManager = initializeApplicationInfoManager(new MyDataCenterInstanceConfig());
        EurekaClient client = initializeEurekaClient(applicationInfoManager, new DefaultEurekaClientConfig());
        return eurekaClient;
    }

//    public void getIns() {
//        DiscoveryManager.getInstance().initComponent(new MyDataCenterInstanceConfig(), new DefaultEurekaClientConfig());
//
//        String vipAddress = "MY-SERVICE";
//
//        InstanceInfo nextServerInfo = null;
//        try {
//            nextServerInfo = DiscoveryManager.getInstance()
//                    .getEurekaClient()
//                    .getNextServerFromEureka(vipAddress, false);
//        } catch (Exception e) {
//            System.err.println("Cannot get an instance of example service to talk to from eureka");
//            System.exit(-1);
//        }
//
//        System.out.println("Found an instance of example service to talk to from eureka: "
//                + nextServerInfo.getVIPAddress() + ":" + nextServerInfo.getPort());
//
//        System.out.println("healthCheckUrl: " + nextServerInfo.getHealthCheckUrl());
//        System.out.println("override: " + nextServerInfo.getOverriddenStatus());
//
//        System.out.println("Server Host Name "+ nextServerInfo.getHostName() + " at port " + nextServerInfo.getPort() );
//    }
}
