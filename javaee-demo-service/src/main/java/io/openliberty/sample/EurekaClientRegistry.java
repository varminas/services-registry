package io.openliberty.sample;

//import com.google.inject.AbstractModule;
//import com.google.inject.Guice;
//import com.google.inject.Injector;
//import com.google.inject.Scopes;
//import com.google.inject.util.Modules;
//import com.netflix.appinfo.ApplicationInfoManager;
//import com.netflix.appinfo.EurekaInstanceConfig;
//import com.netflix.appinfo.InstanceInfo;
//import com.netflix.appinfo.providers.EurekaConfigBasedInstanceInfoProvider;
//import com.netflix.appinfo.providers.MyDataCenterInstanceConfigProvider;
//import com.netflix.discovery.DiscoveryClient;
//import com.netflix.discovery.EurekaClient;
//import com.netflix.discovery.EurekaClientConfig;
//import com.netflix.discovery.guice.EurekaModule;
//import com.netflix.discovery.providers.DefaultEurekaClientConfigProvider;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.enterprise.context.ApplicationScoped;
//import javax.enterprise.inject.Produces;

//@ApplicationScoped
public class EurekaClientRegistry {
//    private static final Logger logger = LoggerFactory.getLogger(EurekaClientRegistry.class);
//
//    private EurekaClient client;
//    private Object lock = new Object();
//
//    @Produces
//    public EurekaClient getEurekaClient() {
//        logger.debug("!!!!!!!!!!!!!getEurekaClient!!!!!!!!!!!!!");
//        if (client == null) {
//            synchronized (lock) {
//                if (client == null) {
//                    logger.debug("client 0");
//                    final Injector injector = Guice.createInjector(Modules.override(new EurekaModule()).with(new AbstractModule() {
//                        @Override
//                        protected void configure() {
//                            bind(EurekaInstanceConfig.class)
//                                    .toProvider(MyDataCenterInstanceConfigProvider.class).in(Scopes.SINGLETON);
//
//                            bind(EurekaClientConfig.class).toProvider(DefaultEurekaClientConfigProvider.class)
//                                    .in(Scopes.SINGLETON);
//
//                            // this is the self instanceInfo used for registration purposes
//                            bind(InstanceInfo.class).toProvider(EurekaConfigBasedInstanceInfoProvider.class)
//                                    .in(Scopes.SINGLETON);
//
//                            bind(EurekaClient.class).to(DiscoveryClient.class).in(Scopes.SINGLETON);
//                            logger.debug("  configure");
//                        }
//                    }));
//
//                    logger.debug("client 1");
//                    client = injector.getInstance(EurekaClient.class);
//                    ApplicationInfoManager.getInstance().setInstanceStatus(InstanceInfo.InstanceStatus.UP);
//                }
//            }
//        }
//        logger.debug("Client {}", client);
//        return client;
//    }
}
