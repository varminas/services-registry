export interface JwtToken {
    id: number;
    name: string;
    value: string;
}
