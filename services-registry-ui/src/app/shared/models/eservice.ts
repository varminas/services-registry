import { JwtToken } from './jwt-token';
import { Health } from './health';

export interface EService {
    id: number;
    name: string;
    description: string;
    url: string;
    jwtTokens: JwtToken[];
    relationsTo: number[];
    health?: Health;
}
