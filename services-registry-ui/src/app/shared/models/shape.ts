import { BehaviorSubject } from 'rxjs';
import { ShapeType } from './shape-type';
import { EService } from './eservice';

export interface Shape {
    service?: EService;
    type: ShapeType;
    config: any;
}
