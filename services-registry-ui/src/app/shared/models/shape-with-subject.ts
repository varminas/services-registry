import { BehaviorSubject } from 'rxjs';
import { ShapeType } from './shape-type';
import { EService } from './eservice';

export interface ShapeWithSubject {
    service?: EService;
    type: ShapeType;
    config: BehaviorSubject<any>;
}
