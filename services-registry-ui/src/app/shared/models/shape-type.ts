export enum ShapeType {
    RECT = 'RECT',
    TEXT = 'TEXT',
    ARROW = 'ARROW',
    TOOLTIP = 'TOOLTIP'
}
