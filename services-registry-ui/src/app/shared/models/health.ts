export interface Health {
    status: HealthStatus;
    relations: Health[];
}

export enum HealthStatus {
    UP = 'UP',
    DOWN = 'DOWN'
}
