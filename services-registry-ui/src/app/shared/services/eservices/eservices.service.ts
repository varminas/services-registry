import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EService } from '@app/shared/models/eservice';
import { HealthStatus } from '@app/shared/models/health';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EServicesService {
  constructor(private http: HttpClient) { }

  getServices(): Observable<EService[]> {
    // return this.http.get<EService[]>(`${environment.apiUrl}/services`);

    const services: EService[] = [
      {
        id: 1,
        name: 'Service 1',
        description: 'Service 1 desc. This is backend service returning users',
        url: 'someurl1.ch',
        jwtTokens: [],
        relationsTo: [2],
        health: {
          status: HealthStatus.UP,
          relations: []
        }
      },
      {
        id: 2,
        name: 'Service 2',
        description: 'Service 2 desc',
        url: 'someurl2.ch',
        jwtTokens: [],
        relationsTo: [3],
        health: {
          status: HealthStatus.UP,
          relations: []
        }
      },
      {
        id: 3,
        name: 'Service 3',
        description: 'Service 3 desc',
        url: 'someurl3.ch',
        jwtTokens: [],
        relationsTo: [5],
        health: {
          status: HealthStatus.UP,
          relations: []
        }
      },
      {
        id: 4,
        name: 'Service 4',
        description: 'Service 4 desc',
        url: 'someurl3.ch',
        jwtTokens: [],
        relationsTo: [5, 7],
        health: {
          status: HealthStatus.UP,
          relations: []
        }
      },
      {
        id: 5,
        name: 'Service 5',
        description: 'Service 5 desc',
        url: 'someurl3.ch',
        jwtTokens: [],
        relationsTo: [],
        health: {
          status: HealthStatus.UP,
          relations: []
        }
      },
      {
        id: 7,
        name: 'Service 6',
        description: 'Service 6 desc',
        url: 'someurl3.ch',
        jwtTokens: [],
        relationsTo: []
      }
    ];
    return of(services);
  }
}
