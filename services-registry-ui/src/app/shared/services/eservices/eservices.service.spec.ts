import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { EServicesService } from './eservices.service';


describe('EServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: EServicesService = TestBed.get(EServicesService);
    expect(service).toBeTruthy();
  });
});
