import { TestBed } from '@angular/core/testing';

import { ShapesService } from './shapes.service';

describe('RelationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShapesService = TestBed.get(ShapesService);
    expect(service).toBeTruthy();
  });
});
