import { Injectable } from '@angular/core';
import { EService } from '@app/shared/models/eservice';
import { Health, HealthStatus } from '@app/shared/models/health';
import { Shape } from '@app/shared/models/shape';
import { ShapeType } from '@app/shared/models/shape-type';
import { ShapeWithSubject } from '@app/shared/models/shape-with-subject';
import Konva from 'konva';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShapesService {

  private readonly RECT_WIDTH = 150;
  private readonly RECT_HEIGHT = 80;
  private readonly RECT_DISTANCE_X = 250;
  private readonly RECT_DISTANCE_Y = 250;
  private readonly MAX_RECT_IN_ROW = 3;
  private readonly ODD_ELEMENT_DIFF = 50;
  private readonly RECT_FILL_COLOR_UP = '#89b717';
  private readonly RECT_FILL_COLOR_DOWN = 'red';

  getShapes(services: EService[]): ShapeWithSubject[] {
    const shapes: Shape[] = [];
    services.forEach((service, index) => {
      const row = Math.trunc(index / this.MAX_RECT_IN_ROW);
      const col = index - this.MAX_RECT_IN_ROW * (row);
      const odd = index % 2;

      const rectX = col * this.RECT_DISTANCE_X;
      const rectY = (row * this.RECT_DISTANCE_Y) + (this.ODD_ELEMENT_DIFF * odd);
      shapes.push({
        service,
        type: ShapeType.RECT,
        config: this.createRect(rectX, rectY, service.health)
      });

      const textX = rectX + 10;
      const textY = rectY + 10;
      shapes.push({
        type: ShapeType.TEXT,
        config: this.createText(textX, textY, service.name)
      });

      const tooltipText = service.description + '\n' +
        'Relations: ' + service.relationsTo.join(', ');

      shapes.push({
        type: ShapeType.TOOLTIP,
        config: this.createTooltip(textX, textY, tooltipText, service.id)
      });
    });

    services.forEach((service) => {
      const relatedFromService = shapes
        .filter(shape => shape.type === ShapeType.RECT)
        .find(shape => shape.service.id === service.id);

      service.relationsTo.forEach(relation => {
        const relatedToService = shapes
          .filter(shape => shape.type === ShapeType.RECT)
          .find(shape => shape.service.id === relation);

        shapes.push({
          type: ShapeType.ARROW, config: this.createArrow(
            relatedFromService.config.x() + relatedFromService.config.width(),
            Math.round(relatedFromService.config.y() + relatedFromService.config.height() / 2),
            relatedToService.config.x(),
            Math.round(relatedToService.config.y() + relatedToService.config.height() / 2)
          )
        });
      });
    });

    return shapes.map(s => {
      return {
        service: s.service,
        type: s.type,
        config: new BehaviorSubject(s.config)
      };
    });
  }

  private createRect(x: number, y: number, health: Health): Konva.Rect {
    return new Konva.Rect({
      x,
      y,
      width: this.RECT_WIDTH,
      height: this.RECT_HEIGHT,
      fill: health != null && health.status === HealthStatus.UP ? this.RECT_FILL_COLOR_UP : this.RECT_FILL_COLOR_DOWN,
      name: 'rect',
      strokeWidth: 2,
      opacity: 0.5,
      draggable: false,
      cornerRadius: 10
    });
  }

  private createText(x: number, y: number, text: string): Konva.Text {
    return new Konva.Text({
      x,
      y,
      text,
      fontSize: 16,
      fontFamily: 'Calibri',
      fill: 'black'
    });
  }

  private createTooltip(x: number, y: number, text: string, serviceId: number): Konva.Text {
    return new Konva.Text({
      fontFamily: 'Calibri',
      fontSize: 12,
      padding: 5,
      textFill: 'white',
      fill: 'black',
      alpha: 0.75,
      text,
      visible: false,
      x,
      y: y + 10,
      serviceId
    });
  }

  private createArrow(x1: number, y1: number, x2: number, y2: number): Konva.Arrow {
    return new Konva.Arrow({
      points: [x1, y1, x2, y2],
      pointerLength: 10,
      pointerWidth: 10,
      fill: 'black',
      stroke: 'black',
      strokeWidth: 3,
      opacity: 0.6
    });
  }
}
