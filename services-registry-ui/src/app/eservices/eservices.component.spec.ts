import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WINDOW_PROVIDERS } from '@app/shared/services/window.service';
import { KonvaModule } from 'ng2-konva';
import { EServicesComponent } from './eservices.component';


describe('EServicesComponent', () => {
  let component: EServicesComponent;
  let fixture: ComponentFixture<EServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [KonvaModule, HttpClientTestingModule],
      declarations: [EServicesComponent],
      providers: [WINDOW_PROVIDERS],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
