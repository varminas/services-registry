import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EService } from '@app/shared/models/eservice';
import { Shape } from '@app/shared/models/shape';
import { ShapeType } from '@app/shared/models/shape-type';
import { ShapesService } from '@app/shared/services/shapes/shapes.service';
import { WINDOW } from '@app/shared/services/window.service';
import { KonvaComponent } from 'ng2-konva';
import { Observable, of, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { EServicesService } from '../shared/services/eservices/eservices.service';

@Component({
  selector: 'app-eservices',
  templateUrl: './eservices.component.html',
  styleUrls: ['./eservices.component.scss']
})
export class EServicesComponent implements OnInit, OnDestroy {
  @ViewChild('stage', { static: false }) stage: KonvaComponent;
  @ViewChild('layer', { static: false }) layer: KonvaComponent;
  @ViewChild('tooltipLayer', { static: false }) tooltipLayer: KonvaComponent;
  @ViewChild('dragLayer', { static: false }) dragLayer: KonvaComponent;

  private destroy$: Subject<void> = new Subject();

  private width: number;
  private height: number;

  services: EService[] = [];
  shapes: Shape[];
  tooltips: Shape[];

  constructor(
    private eservicesService: EServicesService,
    private shapesService: ShapesService,
    @Inject(WINDOW) private window: Window
  ) {
    this.width = this.window.innerWidth - this.window.innerWidth * 0.15;
    this.height = this.window.innerHeight - this.window.innerWidth * 0.15;
  }

  public configStage: Observable<any>;

  ngOnInit(): void {
    this.configStage = of({
      width: this.width,
      height: this.height,
      draggable: true
    });

    this.eservicesService.getServices()
      .pipe(
        takeUntil(this.destroy$),
        map(services => this.shapesService.getShapes(services)),
      ).subscribe(shapes => {
        this.shapes = shapes;
        this.tooltips = shapes.filter(shape => shape.type === ShapeType.TOOLTIP);
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getConfig(shape: Shape): Observable<any> {
    return shape.config;
  }

  overRelation(shape: Shape) {
  }

  get totalServicesCount(): number {
    return this.shapes.filter(shape => shape.type === ShapeType.RECT).length;
  }

  public handleMouseover(ngComponent: any, serviceId: string) {
    const mousePos = this.stage.getStage().getPointerPosition();
    this.layer.getStage().draw();
    document.body.style.cursor = 'pointer';
    ngComponent.config.next({
      opacity: 0.8,
      scaleX: 1.01,
      scaleY: 1.01
    });

    const allTooltips = this.tooltipLayer.getStage().getChildren();
    const matchingTooltipIndex = allTooltips.findIndex(tooltip => tooltip.attrs.serviceId === serviceId);

    allTooltips[matchingTooltipIndex].show();
    this.tooltipLayer.getStage().draw();
  }

  public handleMouseout(ngComponent: any, serviceId: number) {
    this.layer.getStage().draw();
    document.body.style.cursor = 'default';
    ngComponent.config.next({
      opacity: 0.5,
      scaleX: 1,
      scaleY: 1,
    });

    const allTooltips = this.tooltipLayer.getStage().getChildren();
    const matchingTooltipIndex = allTooltips.findIndex(tooltip => tooltip.attrs.serviceId === serviceId);

    allTooltips[matchingTooltipIndex].hide();
    this.tooltipLayer.getStage().draw();
  }
}
