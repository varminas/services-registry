import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KonvaModule } from 'ng2-konva';
import { EServicesComponent } from './eservices.component';
import { EServicesRoutingModule } from './eservices-routing.module';

@NgModule({
  declarations: [EServicesComponent],
  imports: [
    CommonModule,
    KonvaModule,
    EServicesRoutingModule
  ]
})
export class EServicesModule { }
